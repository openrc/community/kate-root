# Maintainer: Nafis <mnabid.25@outlook.com>
# Contributor: David P. <megver83@parabola.nu>
# Contributor: Felix Yan <felixonmars@archlinux.org>
# Contributor: Antonio Rojas <arojas@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>

pkgbase=kate-root
_pkgbase=kate
pkgname=(kwrite-root kate-root)
pkgver=22.04.1
pkgrel=1
arch=(x86_64 i686 armv7h)
license=(GPL LGPL FDL)
makedepends=(extra-cmake-modules kdoctools plasma-framework knewstuff kitemmodels ktexteditor kactivities kuserfeedback)
source=(https://download.kde.org/stable/release-service/$pkgver/src/$_pkgbase-$pkgver.tar.xz
        0001-kate-root.patch
        0001-kwrite-root.patch)
sha256sums=('SKIP'
            'SKIP'
            'SKIP')

prepare() {
  # root shall be allowed once again
  patch -d $_pkgbase-$pkgver -Np1 < 0001-kate-root.patch
  patch -d $_pkgbase-$pkgver -Np1 < 0001-kwrite-root.patch
}

build() {
  cmake -B build -S $_pkgbase-$pkgver \
    -DBUILD_TESTING=OFF
  cmake --build build
}

package_kwrite-root() {
  pkgdesc='Text Editor, patched to be able to run as root'
  groups=(kde-applications kde-utilities)
  url='https://apps.kde.org/kwrite/'
  depends=(ktexteditor hicolor-icon-theme)
  provides=('kwrite')
  conflicts=('kwrite')

  DESTDIR="$pkgdir" cmake --install build

  find "$pkgdir" -type f -name '*kate*' -exec rm {} \;
  rm -r "$pkgdir"/usr/lib/qt/plugins/ktexteditor \
        "$pkgdir"/usr/share/doc/HTML/*/{kate,katepart} \
        "$pkgdir"/usr/share/katexmltools \
        "$pkgdir"/usr/share/locale/*/LC_MESSAGES/{ktexteditorpreviewplugin,lspclient,tabswitcherplugin}.mo \
        "$pkgdir"/usr/share/plasma/plasmoids
}

package_kate-root() {
  pkgdesc='Advanced Text Editor, patched to be able to run as root'
  groups=(kde-applications kde-utilities)
  url='https://apps.kde.org/kate/'
  depends=(knewstuff ktexteditor kactivities kuserfeedback hicolor-icon-theme)
  optdepends=('konsole: open a terminal in Kate'
              'clang: C and C++ LSP support'
              'python-lsp-server: Python LSP support'
              'texlab: LaTeX LSP support'
              'rust: Rust LSP support'
              'git: git-blame plugin')
  provides=('kate')
  conflicts=('kate')

  DESTDIR="$pkgdir" cmake --install build

  find "$pkgdir" -type f -name '*kwrite*' -exec rm {} \;
  rm -r "$pkgdir"/usr/share/doc/HTML/*/kwrite
}
